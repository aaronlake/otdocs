# File Integrity Monitoring (FIM)
## Ensure compliance and data integrity

### What is FIM?
Online Tech's FIM process is a daily file validation check and alerting of files and folders you want to monitor. Online Tech will present a daily report of files with altered credentials, privileges, security settings, and content.

![File Integrity Monitoring](http://www.onlinetech.com/images/diagrams/fim-diagram.png)

### Why enable FIM services?
FIM is enabled partially for piece of mind that changes were made by *you* or someone working on *your* behalf. Alterations to system files or application code are logged and reported so you can ensure the consistency and security of your data.

Additionally the FIM process satisfies multiple compliance requirements from PCI[^fn-pci-compliance]:
> Use file-integrity monitoring or change-detection software on logs to ensure that existing log data cannot be changed without generating alerts (although new data being added should not cause an alert).

FIM is also recommended for additional security to meet HIPAA[^fn-hippa-compliance] compliance:
> Implement hardware, software and/or procedural mechanisms that record and examine activity in information systems that contain or use electronic protected health information.

Other compliance standards such as SOX, NERC SIP, and FISMA benefit from FIM as well.

### What are the benefits?

* It provides additional security with the ability to track change management, including registry changes.
* Provides notifications and alerts that can help you remediate the situation quickly.
* Customizable to monitor specific files – this can be managed by you.
* Fully managed solution. We install, configure, and send the report for you.

### High Profile Cases
* Target Stores (2013): 110m records compromised -- Infection on payment-card readers.
* Home Depot (2014): 56m payment cards -- POS systems infected by masquerading antivirus software.
* Heartland Payment Systems (2008-2009): 130m records compromised -- Malware planted on servers.

[^fn-pci-compliance]: PCI Compliance section 10.5.5

[^fn-hippa-compliance]: HIPAA Compliance §164.312(b) & §164.312\(c\)
