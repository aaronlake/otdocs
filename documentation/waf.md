# Web Application Firewall (WAF)
Protect your web business at the application layer

### What is WAF?
A WAF is a physical device that sits behind your virtual or dedicated firewall and scans incoming website traffic for anything that wouldn't be classified as "business as usual". SQL injections, and other offending traffic is not detectable by standard firewalls or intrusion detection hardware.

![WAF](http://www.onlinetech.com/images/diagrams/waf-diagram.png)

### Why enable WAF services?
A WAF is designed to add another layer to data loss prevention. A baseline for expected website input is established and traffic that falls outside that standard is flagged as potentially harmful. In addition, oversights in application code are automatically filtered to protect your data.

Code review or a Web Application Firewall is a requirement for PCI[^fn-pci-requirement]:
> Verify that a web-application firewall is in place in front of public-facing web
 applications to detect and prevent web-based attacks. - PCI DSS Requirements and
 Security Assessment Procedures, Version 2.0


### What are the benefits?

* Provides an extra layer of protection that a network firewall and IDS cannot
* Can prevent attacks and data exposure before it happens by detecting malicious users and requests for information
* Dynamic profiling to set criteria for accepted traffic based on user behavior
* Can identify malicious sources to stop automated attacks
* Fulfills PCI DSS requirement 6.6 to install a WAF in front of public-facing web applications
* Fully managed solution. Complete web firewall solution fully managed and monitored.

### High Profile Cases
* World Trade Organization (2015): 53k personal data
* SAP (2015): Detected by security audit
* Tesla (2014): Detected by white hat security group
* US Federal (2013): >100,000 user information
* Yahoo (2012): 450k passwords stored in plain text

[^fn-pci-requirement]: Section (6.6)
