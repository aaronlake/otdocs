# Daily Log Review
## Monitor User and System Activity

### What is Daily Log Review?
Daily Log Review is centralized monitoring of failed and successful login attempts, system activity, and the detection of abnormalities in the daily workflow. Online Tech stores 90 days of instant access logs, and up to a year of archived messages, as required by PCI and HIPAA compliance.

![Daily Log Review](http://www.onlinetech.com/images/stories/misc/dailylogreview.png)

### Why enable Daily Log Review?
Tracking user login attempts and system abnormalities greatly increases system security beyond a standard firewall or two-factor authentication. In addition Daily Log Review is a requirement of PCI and HIPAA:

PCI Requirement 10.3:
> Record at least the following audit trail entries for all system components for each event - a whole list of events follow, including user ID, type of event, data and time, success or failure indication, etc.

PCI requirement 10.6:
> Review logs for all system components at least daily. Log reviews must include those servers that perform security functions like intrusion-detection system (IDS) and authentication, authorization, and accounting protocol (AAA) servers (for example, RADIUS).

HIPAA requires the ability to monitor log-in attempts and reporting discrepancies[^fn-hipaa-reference] of the HIPAA Security Standards Administrative Safeguards).

### What are the benefits?
* A system that analyzes and condenses logging data for ease of review.
* Instead of auditing devices after an issue is raised, ongoing daily log review allows the client to view changes to their system daily.
* Allows the client to be more proactive in preventing and resolving issues.
* Decreases a company’s risk of security breaches, malware, loss and legal liabilities.
* Fully managed solution. Centralized logging configured and managed by Online Tech.

### High Profile Cases
* Sony Online Entertainment (2011): 102m records compromised
* Anthem (2015): 69-80m records compromised
* Living Social (2013): 50m records compromised
* TJX Companies Inc (2006-2007): 46m records compromised

[^fn-hipaa-reference]: HIPAA Discrepancies (§164.308(a)(5)(ii)(C)
