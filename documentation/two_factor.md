# Two-factor Authentication
## Protection from credential theft & weak passwords

### What is Two-factor Authentication
With dozens of high profile security breaches yearly and user tendancy to reuse passwords, Two-factor Authentication provides an additional security layer to access your company's environment. A small application is installed on your users' phones which supplies a randomly generated password keyed to that specific user's account to suplement a standard username and password pair.
![Two-factor Authentication](http://www.onlinetech.com/images/diagrams/two-factor-authentication-diagram.png)

### Why enable Two-factor Authentication?
Two-factor authentication adds another layer of security for users logging in to your enviornment beyond a username and password combination. Common passwords, reused passwords, and malware infected machines are three of the most frequent security breaches. Requiring complex passwords (combination of uppercase and numbers) can help ensure security, but adding a second layer of authentication via two-factor ensures that your remote users are, in fact, your users.

### What are the benefits?
* Ease of integration and installation
* Can be controlled and implemented by the client
* Inexpensive but adds a significantly higher level of security
* Meets regulatory compliance requirements for sensitive data protection
* Supports any type of phone, including smartphones, features and landlines

### High Profile Cases
* JPMorgan Chase (2014): 83m customer records
* Slack (2014): 1m customer usernames and passwords
